class RelationshipsController < ApplicationController
	
  def show
    
    @otheruser =  User.find_by_username(params[:name])
    @reviews  =  @otheruser.write_reviews.all
    
    @relationship = Relationship.where(
    follower_id: current_user.id,
    followed_id: @otheruser.id
).first_or_initialize if current_user
    
	end	
  
  
	def create
	   @relationship = Relationship.new
        @relationship.followed_id = params[:followed_id]
        @relationship.follower_id = current_user.id
 
    if @relationship.save
        redirect_to followuser_url(name: params[:name])
    else
        flash[:error] = "Couldn't Follow"
        redirect_to root_url
    end
  end

  def destroy
  	@relationship = Relationship.where(follower_id: current_user.id, id: params[:id]).first_or_initialize
    @relationship.destroy
    
    
    redirect_to followuser_url(name: params[:name])
  end
 
 
end
