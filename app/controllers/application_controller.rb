class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
 before_filter :configure_permitted_parameters, if: :devise_controller?
 	
  def configure_permitted_parameters
    devise_parameter_sanitizer.for(:sign_up) { |u| u.permit( :email, :username,:password, :password_confirmation, :remember_me, :phno) }
    devise_parameter_sanitizer.for(:sign_in) { |u| u.permit( :username, :email, :password, :remember_me,:phno) }
    devise_parameter_sanitizer.for(:account_update) { |u| u.permit(:phno,:email,:username,:phno) }
  end

  def after_sign_in_path_for(resource_or_scope)
  	if resource_or_scope.is_a?(AdminUser) 
      admin_dashboard_path(resource_or_scope)
    else
    if current_user.phno.blank?
   #   user_id  = current_user.id
  phonenum_path
   else 
    root_path
  end
end
end
end
