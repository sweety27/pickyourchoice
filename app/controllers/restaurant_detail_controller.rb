class RestaurantDetailController < ApplicationController
	def restaurant_details
	   @restresult = RestaurantSearchResult.find_by_id(params[:id])
    	respond_to do |format|
    	format.html
    	format.json { render json: @restresult }
		end
	end
end
