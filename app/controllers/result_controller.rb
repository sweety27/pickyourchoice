class ResultController < ApplicationController
  def index()
  	@locations  = Location.where('name LIKE ?', "%#{params[:address]}%")
    respond_to do |format|
    format.html
    format.json { render json: @locations }
    @search_results = SearchResult.all
    
  end
  end
  def pgresult
    @pgresults = []
   wrev = {} 
   wrev["wrev"] = ""
  wrev["wrev"]  = SearchResult.all
   @pgresults << wrev
       wrev = {}
    respond_to do |format|
    format.html
    format.json { render json: @pgresults }
  end
  end  
  def pgresultbyfoodtype
   @pgresults = []
   wrev = {} 
   wrev["wrev"] = ""
   wrev["meal"] = ""
   wrev["sharing"] = ""
   query_string = String.new
   query_values = []
   
     if  params[:sharing_type].nil? and params[:filter_type].nil?
         wrev["wrev"]  = SearchResult.all
         @pgresults << wrev
         wrev = {}
         
     end

     if params[:sharing_type]  != nil and params[:filter_type].nil?
      params[:sharing_type].each do |sharing_type|
       wrev["wrev"]  = SearchResult.by_sharingtype( sharing_type) 
       @pgresults << wrev
       wrev = {}
     end
      end
       
    if params[:filter_type] != nil and params[:sharing_type]  != nil
    params[:filter_type].each do |filter_type|
        if filter_type == "desending"
     params[:sharing_type].each do |sharing_type|
        wrev["wrev"]  = SearchResult.by_sharingtype( sharing_type).order("price desc")
        @pgresults << wrev
        wrev = {}
       end 
       else
       params[:sharing_type].each do |sharing_type|
      wrev["wrev"]  = SearchResult.by_sharingtype( sharing_type) 
       @pgresults << wrev
       wrev = {}
     end
  end
      if params[:sharing_type].nil? and params[:filter_type] != nil
        params[:filter_type].each do |filter_type|
        if filter_type == "desending"
         wrev["wrev"]  = SearchResult.order("price desc")
         @pgresults << wrev
        wrev = {}
        end
      end
      end

end
end
    respond_to do |format|
    format.html
    format.json { render json: @pgresults }
  end
  end
  def new
  end 
  def details
    @user = User.new

  end
  def reviewdetails
    @users = User.all
   @total = [] #each element of this array would contain a hash of _'user, reviews[]'_
wrev = {}  # a hash that will contain the user, and its write reviews
wrev["user"] = ""
wrev["email"] = ""
wrev["followers"] = 0
wrev["user_images"] = ""
wrev["wrev"] = "" #reviews are an array 
wrev["likes"] = 0
@likes = WriteReview.new
@users.each do |u|
 wrev["user"] = u.username
 wrev["email"] = u.email
 wrev["followers"] = u.followers.count
 wrev["user_images"] = u.user_images

 wrev["wrev"] = u.write_reviews.last
 @likes = u.write_reviews.all
 @likes.each do |h|
  wrev["likes"] = h.get_likes.size
  wrev["dislikes"] = h.get_dislikes.size
 end
 
 @total << wrev 
 wrev = {}
end

respond_to do |format|
 format.html
 format.json { render json: @total }
end
  
  end
  def completereview
  end
  def write_review
  	@writereview = WriteReview.new
  end
  def create 
     @writereview = WriteReview.new(user_params)

   if @writereview.save!
   redirect_to root_path
  end
   end
 def like
@writereview = WriteReview.find(params[:id])
@writereview.liked_by current_user
  Notification.create(
             write_review_id: @writereview.id,
            user_id: current_user.id,
             read: false,
             text: (current_user.username + " likes you comment")
              )

      @users = User.all
   @total = [] #each element of this array would contain a hash of _'user, reviews[]'_
wrev = {}  # a hash that will contain the user, and its write reviews
wrev["user"] = ""
wrev["email"] = ""
wrev["followers"] = 0
wrev["user_images"] = ""
wrev["wrev"] = "" #reviews are an array 
wrev["likes"] = 0
@likes = WriteReview.new
@users.each do |u|
 wrev["user"] = u.username
 wrev["email"] = u.email
 wrev["followers"] = u.followers.count
 wrev["user_images"] = u.user_images

 wrev["wrev"] = u.write_reviews.last
 @likes = u.write_reviews.all
 @likes.each do |h|
  wrev["likes"] = h.get_likes.size
  wrev["dislikes"] = h.get_dislikes.size
 end
 
 @total << wrev 
 wrev = {}
end

respond_to do |format|
 format.html
 format.json { render json: @total }
end 
    
end

def dislike
@writereview = WriteReview.find(params[:id])
@writereview.disliked_by current_user
Notification.create(
             write_review_id: @writereview.id,
            user_id: current_user.id,
             read: false,
             text: (current_user.username + " dislikes you comment")
              )
 @users = User.all
    @total = [] #each element of this array would contain a hash of _'user, reviews[]'_
wrev = {}  # a hash that will contain the user, and its write reviews
wrev["user"] = ""
wrev["email"] = ""
wrev["followers"] = 0
wrev["user_images"] = ""
wrev["wrev"] = "" #reviews are an array 
wrev["likes"] = 0
@likes = WriteReview.new
@users.each do |u|
 wrev["user"] = u.username
 wrev["email"] = u.email
 wrev["followers"] = u.followers.count
 wrev["user_images"] = u.user_images

 wrev["wrev"] = u.write_reviews.last
 @likes = u.write_reviews.all
 @likes.each do |h|
  wrev["likes"] = h.get_likes.size
  wrev["dislikes"] = h.get_dislikes.size
 end
 
 @total << wrev 
 wrev = {}
end

respond_to do |format|
 format.html
 format.json { render json: @total }
end
end

   private
  
    def user_params
       params.require(:write_review).permit(:clean,:food,:locality,:behavior,:amenity,:likes,:dislikes,:comment,:user_id)
    end 
end
