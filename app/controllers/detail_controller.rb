class DetailController < ApplicationController
	def details
	   @pgresults = SearchResult.find_by_id(params[:id])
    	respond_to do |format|
    	format.html
    	format.json { render json: @pgresults }
	end
end
end
