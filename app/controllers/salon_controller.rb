class SalonController < ApplicationController
  def index()
  	@locations  = Location.where('name LIKE ?', "%#{params[:address]}%")
    respond_to do |format|
    format.html
    format.json { render json: @locations }
    end
  end
  def salonresult
  	@salonresults = SalonSearchResult.all
    respond_to do |format|
    format.html
    format.json { render json: @salonresults }
    end
  end
end
