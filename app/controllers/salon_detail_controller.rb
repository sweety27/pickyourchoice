class SalonDetailController < ApplicationController
	def salon_details
	   @salonresult = SalonSearchResult.find_by_id(params[:id])
    	respond_to do |format|
    	format.html
    	format.json { render json: @salonresult }
		end
	end
end
