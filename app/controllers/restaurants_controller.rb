class RestaurantsController < ApplicationController
  def index()
  	@locations  = Location.where('name LIKE ?', "%#{params[:address]}%")
    respond_to do |format|
    format.html
    format.json { render json: @locations }
    end
  end
  def restaurantresult
  	@restaurantresults = RestaurantSearchResult.all
    respond_to do |format|
    format.html
    format.json { render json: @restaurantresults }
    end
  end
  def new
  end
  def restwritereview
    @writerestreview = WriteRestaurantReview.new
  end
  def create 
    @writerestreview = WriteRestaurantReview.new(user_rest_params)
    if @writerestreview.save!
      redirect_to root_path
    end
  end
  private
  def user_rest_params
    params.require(:restwritereview).permit(:Quality,:ServiceStandards,:Ambience,:Reason,:name,:email,:user_id)
  end 
end
