$.get('resultlocation.json', function(data){
  $("#name").typeahead({ source:data });
},'json');

$.get('resultlocation.json', function(data){
  $("#rest_name").typeahead({ source:data });
},'json');

myApp = angular.module('myApp', ['ngAnimate', 'ui.bootstrap']);
myApp.controller('filterController',function($scope,$http){
  $scope.user = {};
   $http.get('./location.json').success(function(data) {
  $scope.locations = data;
});
  $scope.genders = [{
    id:'male',
    desc:'male'
  },{
    id:'female',
    desc:'female'
  }];
	$scope.ranges = [{
		id:'5000-6000',
		desc:'5000-6000'
		},{
		id:'6000-8000',
		desc:'6000-8000'
		},{
		id:'8000-above',
		desc:'8000-above'	
	}];
	$scope.cuisines = [{
		id:'NI',
		desc : 'North Indian'
		},{
		id:'SI',
		desc : 'South Indian'
		},{
		id:'Ch',
		desc:'Chinese'
		},{
		id:'Cafe',
		desc:'Cafes'
		},{
		id:'Lun',
		desc:'Lunch'
		},{
		id:'Din',
		desc:'Dinner'
	}];
	$scope.nearbyimages = [
      {
        id: 1,
        img:'rest_img.png',
        name:'Restaurant'
      },{
        id: 2,
        img:'salon_img.png',
        name:'Salon'
      },{
        id: 3,
        img:'mall_img.png',
        name:'Mall'
      }
      ,{
        id: 4,
        img:'gym_img.png',
        name:'Gym'
      }
  	];

  $scope.salontypes = [{
    id:"Uni",
    desc:"Unisex"
  },{
    id:"male",
    desc:"Male"
  },{
    id:"female",
    desc:"Female"
  }]; 
  $scope.gymtypes = [{
    id:"Uni",
    desc:"Unisex"
  },{
    id:"male",
    desc:"Male"
  },{
    id:"female",
    desc:"Female"
  }]; 
  	//$scope.divShow = 1;
  	$scope.select = function(arg){
  		$scope.divShow = arg;
  		$scope.selected = arg; 

  	}
  	$scope.isActive = function(arg) {
        return $scope.selected === arg;
 	}; 
    $scope.searchResult = function(){
      alert("Search Result");
    };
    $scope.SearchPg = function(location,gender,amount){
      window.location.href= "/result?location=" + location + "&gender=" + gender + "&amount=" + amount;
      var location_id = angular.element( document.querySelector('#name'));
      location_id = location;
      console.log(location_id);
    }
});
myApp.controller('reviewController',function($scope,$http){
  $http.get('./review.json').success(function(data) {
  $scope.reviewDetails = data;
  $scope.gotoFollower = function(dataname){
    
    location.href = "/followuser?name=" + dataname;
  }
});
	$scope.gotolikes = function(dataname){
    
    $http.get('./likes.json?id=' + dataname).success(function(data){
     $scope.reviewDetails = data; 

    });
  }
  $scope.gotodislikes = function(dataname){
    
    $http.get('./dislikes.json?id=' + dataname).success(function(data){
     $scope.reviewDetails = data; 

    });
  }
	$scope.quantity = 4;
	$scope.limit = 10;
    $scope.loadMore = function() {
       $scope.limit += 10;
    };

});

var $scope, $location;
myApp.service('anchorSmoothScroll', function(){
    
    this.scrollTo = function(eID) {
        var startY = currentYPosition();
        var stopY = elmYPosition(eID);
        var distance = stopY > startY ? stopY - startY : startY - stopY;
        if (distance < 100) {
            scrollTo(0, stopY); return;
        }
        var speed = Math.round(distance / 100);
        if (speed >= 20) speed = 20;
        var step = Math.round(distance / 25);
        var leapY = stopY > startY ? startY + step : startY - step;
        var timer = 0;
        if (stopY > startY) {
            for ( var i=startY; i<stopY; i+=step ) {
                setTimeout("window.scrollTo(0, "+leapY+")", timer * speed);
                leapY += step; if (leapY > stopY) leapY = stopY; timer++;
            } return;
        }
        for ( var i=startY; i>stopY; i-=step ) {
            setTimeout("window.scrollTo(0, "+leapY+")", timer * speed);
            leapY -= step; if (leapY < stopY) leapY = stopY; timer++;
        }
        
        function currentYPosition() {
            // Firefox, Chrome, Opera, Safari
            if (self.pageYOffset) return self.pageYOffset;
            // Internet Explorer 6 - standards mode
            if (document.documentElement && document.documentElement.scrollTop)
                return document.documentElement.scrollTop;
            // Internet Explorer 6, 7 and 8
            if (document.body.scrollTop) return document.body.scrollTop;
            return 0;
        }
        
        function elmYPosition(eID) {
            var elm = document.getElementById(eID);
            var y = elm.offsetTop;
            var node = elm;
            while (node.offsetParent && node.offsetParent != document.body) {
                node = node.offsetParent;
                y += node.offsetTop;
            } return y;
        }

    };
    
});

myApp.controller('ScrollCtrl', function($scope, $location, anchorSmoothScroll) {
    $scope.gotoElement = function (eID){
      anchorSmoothScroll.scrollTo(eID)
      ;
    };
  });

myApp.controller('FollowerController',function($scope,$http){

  $scope.followerProfileDetails = [
  {
    reviewer:'Ranveer Sharma',
    caption:'Caption by the user',
    title:'Title given to the review on the basis of reviews given',
    reviewerFollower:'5',
    reviewsInTotal:'10',
    reviewerImage:'default.jpg',
  }];
   $scope.suggestedFollowerProfileDetails = [
  {
    reviewer:'Kavita Pathak',
    title:'Title given to the review on the basis of reviews given',
    reviewerFollower:'5',
    reviewsInTotal:'10',
    reviewerImage:'default.jpg',
  }];
  $scope.photos=[{
    image:'follower-photo1',
    caption:'Location and other details'
  },{
    image:'follower-photo2',
    caption:'Location and other details'
  },{
    image:'follower-photo3',
    caption:'Location and other details'
  }];
  $scope.followerCategories =['Reviews','Followers','Photos'];
  $scope.divShow = 'Reviews';
  $scope.select = function(arg){
    $scope.divShow = arg;
  }
});

var uniqueItems = function (data, key) {
    var result = [];
    for (var i = 0; i < data.length; i++) {
        var value = data[i][key];
        if (result.indexOf(value) == -1) {
            result.push(value);
        }
    
    }
    return result;
};

myApp.controller('filterPgController',function($scope,$http) {
    $scope.useType = {};
    $scope.useShare = {};
    $scope.useFoodpref= {};
    $scope.useMeal= {};
    $http.get('./pgresults.json').success(function(data) {
      $scope.pgDetails = data;
      $scope.filteredPg = $scope.pgDetails;
    
});    

$scope.gotoDetails = function(id){
  location.href = "/details?id=" + id;
}
   
        
  $scope.sortGroup = [{
    groupid: "filter_type",
    id:"",
    type:"Most Reviewed"
  },{
    groupid: "filter_type",
    id:"featured",
    type:"Featured Roofs"
  },{
    groupid: "filter_type",
    id:"desending",
    type:"Price: High to Low"
  },{
    groupid: "filter_type",
    id:"asending",
    type:"Price: Low to High"
  }]; 
   
        

        

  $scope.shareGroup = [{
    groupid: "sharing_type",
    id:"single",
    type:"Individual"
  },{
    groupid: "sharing_type",
    id:"Two-Sharing",
    type:"Two Sharing"
  },{
    groupid: "sharing_type",
    id:"Three-Sharing",
    type:"Three Sharing"
  },{
    groupid: "sharing_type",
    id:"Four-Sharing",
    type:"Four Sharing"
  },
  {
    groupid: "sharing_type",
    id:"Five-Sharing",
    type:"Five Sharing"
  }]; 


   $scope.selection=[];
   $scope.meal=[];
   $scope.share=[];
   $scope.filter=[];
   
   $scope.getresultbyfood = function(dataname,datatype){
   
   


    if(datatype == "sharing_type")  {
       
     var idx = $scope.share.indexOf(dataname);
     if (idx > -1) {

       $scope.share.splice(idx, 1);
     }

     else 
     {
        $scope.share.push(dataname);
     }
    
    }
    if(datatype == "filter_type")  {
       
     var idx = $scope.filter.indexOf(dataname);
     if (idx > -1) {

       $scope.filter.splice(idx, 1);
     }

     else 
     {
        $scope.filter.push(dataname);
     }
    
    }
    
    $http({ url: 'pgresultsbyfood.json', method: "GET", params: { "sharing_type[]": $scope.share ,"filter_type[]": $scope.filter   } }).success(function(data){
    
     $scope.resultdata = data; 
     $scope.filteredPg = $scope.resultdata; 
    });     
    }   
});

myApp.controller('filterRestaurantController',function($scope,$http){
    $http.get('./restaurantresults.json').success(function(data) {
      $scope.restDetails = data;
    });  

    $scope.gotoRestDetails = function(id){
      location.href = "/restaurant_details?id=" + id;
    }
});


myApp.controller('filterSalonController',function($scope,$http){
    $http.get('./salonresults.json').success(function(data) {
      $scope.salonDetails = data;
    });  

    $scope.gotoSalonDetails = function(id){
      location.href = "/salon_details?id=" + id;
    }
});


