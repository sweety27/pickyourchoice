ActiveAdmin.register SalonSearchResult do

# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
permit_params :salonname,:salonaddress,:work_hours,:work_days,:gender,:add_comment,:pre_booking,:salon_price,:location_name,:phone,:phone1,:salon_images1,:salon_images2,:salon_images3,:salon_images4
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if resource.something?
#   permitted
# end

form multipart: true  do |f|
f.input :salonname, :required => false, :label => "Name  "
f.input :gender, :required => false, :label => "Salon For :", :as => :select ,:collection => %w{ Male Female Female&Kids All }, :prompt => "Select Gender"
    f.input :salonaddress, :required => false, :label => " Address "
    f.input :location_name, :required => false, :label => " Location "
   
    f.input :salon_price, :required => false, :label => "Price/Fee  "
    
    
    f.input :phone, :required => false, :label => "Contact 1"

    f.input :phone1, :required => false, :label => "Contact 2 "
     f.input :work_hours, :required => false, :label => "Working Hours"
     f.input :work_days, :required => false, :label => "Working Days "
     f.input :pre_booking, :required => false, :label => "Pre-Booking Avail ", :as => :select ,:collection => %w{ Yes No }, :prompt => "Select Option"
     f.input :add_comment, :required => false, :label => "Addtional Info "
     f.input :salon_images1, :required => false, :as => :file, :label => "Image  "
     f.input :salon_images2, :required => false, :as => :file, :label => "Image  "
     f.input :salon_images3, :required => false, :as => :file, :label => "Image  "
     f.input :salon_images4, :required => false, :as => :file, :label => "Image  "
     f.actions
   end 
end
