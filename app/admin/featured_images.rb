
ActiveAdmin.register FeaturedImage do

# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
permit_params :image,:pg_images
form multipart: true  do |f|
  
    
    f.input :pg_images, :required => false, :as => :file
    # Will preview the image when the object is edited
  
  f.actions
 end
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if resource.something?
#   permitted
# end


end
