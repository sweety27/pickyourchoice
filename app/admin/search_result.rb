ActiveAdmin.register SearchResult do

# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
 permit_params :gender,:pgname,:pgadress,:secdeposit,:work_hours,:amenties,:landmark,:add_comment,:phone1,:phone2,:location_name,:pg_images ,:price,:price1,:price2,:price3,:price4,:sharing_type,:sharing_type1,:sharing_type2,:sharing_type3,:sharing_type4,:food_type,:pg_images1,:pg_images2,:pg_images3,:pg_images4,:pg_images5,:meals_type,:location_cd,:sharing_type1 ,:sharing_tyep2,:sharing_tyep3,:sharing_tyep4
form multipart: true  do |f|
  
    
    f.input :gender, :as => :select ,:collection => %w{ Male Female }, :prompt => "Select Gender" ,:label =>"Gender"
    f.input :pgname, :required => false, :label => "PG Name"
    f.input :pgadress, :required => false, :label => "PG Address"
    f.input :phone1, :required => false, :label => "Contact 1"
    f.input :phone2, :required => false, :label => "Contact 2"
    f.input :secdeposit, :required => false, :label => "PG Deposit"
    
    f.input :location_cd, :required => false, :label => "Location Code"
    f.input :location_name, :required => false, :label => "Location Name"
    f.input :landmark, :required => false, :label => "Landmark"
    
    f.input :food_type, :required => false, :label => "Food Type" , :as => :select  ,:collection => %w{North-Indian South-Indian Both }, :prompt => "Select Foodtype"
    f.input :meals_type, :required => false, :label => "Meals Type",:collection => %w{Breakfast Lunch Dinner Lunch&Dinner All }, :prompt => "Select Mealstype"
    
    
    f.input :sharing_type, :label => "Sharing type" ,:as => :select ,:collection => %w{Single Two-Sharing Three-Sharing Four-Sharing Five-Sharing}, :prompt => "Select Sharing type" 
    
    f.input :price, :required => false, :label => "Price"


    f.input :sharing_type1, :label => "Sharing type" ,:as => :select ,:collection => %w{Single Two-Sharing Three-Sharing Four-Sharing five-sharing}, :prompt => "Select Sharing type"
    f.input :price1, :required => false, :label => "Price"

    f.input :sharing_tyep2, :label => "Sharing type" ,:as => :select ,:collection => %w{Single Two-Sharing Three-Sharing Four-Sharing five-sharing}, :prompt => "Select Sharing type"
    f.input :price2, :required => false, :label => "Price"

    f.input :sharing_tyep3, :label => "Sharing type" ,:as => :select ,:collection => %w{Single Two-Sharing Three-Sharing Four-Sharing five-sharing}, :prompt => "Select Sharing type"
    f.input :price3, :required => false, :label => "Price"

    f.input :sharing_tyep4, :label => "Sharing type" ,:as => :select ,:collection => %w{Single Two-Sharing Three-Sharing Four-Sharing five-sharing}, :prompt => "Select Sharing type"
    f.input :price4, :required => false, :label => "Price"




    f.input :work_hours, :required => false, :label => "Opening/GateClosing Time"
    f.input :amenties, :required => false, :label => "Amenties"
    f.input :add_comment, :required => false, :label => "Addtional Info"
    f.input :pg_images, :required => false, :as => :file, :label => "Image"
    f.input :pg_images1, :required => false, :as => :file, :label => "Image"
    f.input :pg_images2, :required => false, :as => :file, :label => "Image"
    f.input :pg_images3, :required => false, :as => :file, :label => "Image"
    f.input :pg_images4, :required => false, :as => :file, :label => "Image"
    f.input :pg_images5, :required => false, :as => :file, :label => "Image"
    
      

    # Will preview the image when the object is edited
  
  f.actions
 end
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if resource.something?
#   permitted
# end


end
