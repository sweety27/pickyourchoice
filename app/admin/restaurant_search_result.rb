ActiveAdmin.register RestaurantSearchResult do

# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
permit_params :restname,:restaddress,:restcostfortwo,:buffet_avail,:buffet_price,:bar_avail,:food_categories,:work_hours,:work_days,:add_comment,:resttype,:pre_booking,:rating,:phone1,:phone2,:location_name,:res_images1,:res_images2,:res_image3,:res_images4,:res_images5
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if resource.something?
#   permitted
# end
form multipart: true  do |f|
f.input :restname, :required => false, :label => "Name  "
    f.input :restaddress, :required => false, :label => " Address "
    f.input :location_name, :required => false, :label => " Location "
   
    f.input :restcostfortwo, :required => false, :label => "Cost For Two "
    f.input :rating, :required => false, :label => "Rating "
    f.input :resttype, :required => false, :label => "Type " , :as => :select ,:collection => %w{ Veg Non-Veg Both}, :prompt => "Select Option"
    f.input :food_categories, :required => false, :label => "Categories Avail " 
    f.input :bar_avail, :required => false, :label => "Bar Avail " , :as => :select ,:collection => %w{Yes No}, :prompt => "Select Option"
    f.input :phone1, :required => false, :label => "Contact 1"
    f.input :phone2, :required => false, :label => "Contact 2 "
     f.input :work_hours, :required => false, :label => "Working Hours"
     f.input :work_days, :required => false, :label => "Working Days "
     f.input :pre_booking, :required => false, :label => "Pre-Booking Avail ", :as => :select ,:collection => %w{ Yes No }, :prompt => "Select Option"
      f.input :buffet_avail, :required => false, :label => "Buffet Avail ", :as => :select ,:collection => %w{ Yes No }, :prompt => "Select Option"
     f.input :buffet_price, :required => false, :label => "Buffet Price If Avail "
     f.input :add_comment, :required => false, :label => "Addtional Info "
     f.input :res_images1, :required => false, :as => :file, :label => "Image  "
     f.input :res_images2, :required => false, :as => :file, :label => "Image  "
     f.input :res_images3, :required => false, :as => :file, :label => "Image  "
     f.input :res_images4, :required => false, :as => :file, :label => "Image  "
     f.input :res_images5, :required => false, :as => :file, :label => "Image  "
     f.actions
   end 

end
