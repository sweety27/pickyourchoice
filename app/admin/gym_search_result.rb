ActiveAdmin.register GymSearchResult do

# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
permit_params :gymname,:gymaddress,:gymcost,:work_hours,:work_days,:add_comment,:location_name,:anual_fee,:halfyearly_fee,:gymtype,:phone,:phone1,:gym_images1,:gym_images2,:gym_images3,:gym_images4
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if resource.something?
#   permitted
# end
form multipart: true  do |f|
f.input :gymname, :required => false, :label => "Name"
    f.input :gymaddress, :required => false, :label => "GYM Address"
     f.input :location_name, :required => false, :label => "Location"
    f.input :gymcost, :required => false, :label => "Monthly Fee"
    f.input :anual_fee, :required => false, :label => "6 Months Subscription fee"
    f.input :halfyearly_fee, :required => false, :label => "Yearly Subscriptipn fee"
    f.input :gymtype, :required => false, :label => "GYM type"
    f.input :phone, :required => false, :label => "Contact 1"
    f.input :phone1, :required => false, :label => "Contact 2"
     f.input :work_hours, :required => false, :label => "Working Hours"
     f.input :work_days, :required => false, :label => "Working Days "
     f.input :add_comment, :required => false, :label => "Addtional Info "
     f.input :gym_images1, :required => false, :as => :file, :label => "Image"
     f.input :gym_images2, :required => false, :as => :file, :label => "Image"
     f.input :gym_images3, :required => false, :as => :file, :label => "Image"
     f.input :gym_images4, :required => false, :as => :file, :label => "Image"
     f.actions
   end 
end
