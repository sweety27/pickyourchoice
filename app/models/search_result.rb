class SearchResult < ActiveRecord::Base
	has_attached_file :pg_images,:styles => {
      :thumb => "100x100#",
      :small  => "150x150>",
      :medium => "400x200" }, default_url: "/images/missing.png"
  validates_attachment :pg_images,
  content_type: { content_type: ["image/jpeg", "image/gif", "image/png"] }
  has_attached_file :pg_images1,:styles => {
      :thumb => "100x100#",
      :small  => "150x150>",
      :medium => "400x200" }, default_url: "/images/missing.png"
  validates_attachment :pg_images1,
  content_type: { content_type: ["image/jpeg", "image/gif", "image/png"] }
  has_attached_file :pg_images2,:styles => {
      :thumb => "100x100#",
      :small  => "150x150>",
      :medium => "400x200" }, default_url: "/images/missing.png"
  validates_attachment :pg_images2,
  content_type: { content_type: ["image/jpeg", "image/gif", "image/png"] }
  has_attached_file :pg_images3,:styles => {
      :thumb => "100x100#",
      :small  => "150x150>",
      :medium => "400x200" }, default_url: "/images/missing.png"
  validates_attachment :pg_images3,
  content_type: { content_type: ["image/jpeg", "image/gif", "image/png"] }
  has_attached_file :pg_images4,:styles => {
      :thumb => "100x100#",
      :small  => "150x150>",
      :medium => "400x200" }, default_url: "/images/missing.png"
  validates_attachment :pg_images4,
  content_type: { content_type: ["image/jpeg", "image/gif", "image/png"] }
  has_attached_file :pg_images5,:styles => {
      :thumb => "100x100#",
      :small  => "150x150>",
      :medium => "400x200" }, default_url: "/images/missing.png"
  validates_attachment :pg_images5,
  content_type: { content_type: ["image/jpeg", "image/gif", "image/png"] }
  has_attached_file :pg_images6,:styles => {
      :thumb => "100x100#",
      :small  => "150x150>",
      :medium => "400x200" }, default_url: "/images/missing.png"
  validates_attachment :pg_images6,
  content_type: { content_type: ["image/jpeg", "image/gif", "image/png"] }
  
  def self.by_sharingtype(sharing_type)
      return all unless sharing_type.present?
      
    where("sharing_type = ? OR sharing_type1 = ? OR sharing_tyep2 = ? OR sharing_tyep3 = ? OR sharing_tyep4 = ?" ,sharing_type,sharing_type,sharing_type,sharing_type,sharing_type)
   
  end

  def self.by_sorttype(filter_type)
      return all unless filter_type.present?
      if  filter_type == "desending"

          order(price: :desc)
         
       end
    end
end
