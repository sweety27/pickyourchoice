class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable , :omniauthable 
  acts_as_voter       
 has_attached_file :user_images,:styles => {
      :thumb => "100x100#",
      :small  => "150x150>",
      :medium => "200x200" }, default_url: "/assets/default.jpg"
  validates_attachment :user_images,
  content_type: { content_type: ["image/jpeg", "image/gif", "image/png"] }

   def self.from_omniauth(auth)
  where(provider: auth.provider, uid: auth.uid).first_or_initialize do |user|
    user.provider = auth.provider
    user.uid  = auth.uid
    user.email = auth.info.email
    user.password = Devise.friendly_token[0,20]
    user.username = auth.info.name   # assuming the user model has a name
    user.save!
    
  end
end	 
def self.new_with_session(params, session)
  
    super.tap do |user|
      if data = session["devise.facebook_data"] && session["devise.facebook_data"]["extra"]["raw_info"]

        user.email = data["email"] if user.email.blank?
      end
    end
  end
   def password_required? 
    super && provider.blank?

  end
  def email_required?
   super && provider.blank?
  end   
  has_many :notifications
  has_many :write_reviews, dependent: :destroy
  has_many :relationships ,dependent: :destroy
  has_many :follower_relationships, class_name: "Relationship", foreign_key: "followed_id"
  has_many :followed_relationships, class_name: "Relationship", foreign_key: "follower_id"
  has_many :followers, through: :follower_relationships
  has_many :followeds, through: :followed_relationships

  def follow(other_user,user)
  Relationship.create(user_id: other_user.id,follower_id: user)
  end

  # Unfollows a user.
  def unfollow(other_user)
    relationships.find_by(user_id: other_user.id).destroy
  end

  # Returns true if the current user is following the other user.
  def following?(other_user)
    followeds.include?(other_user)
  end
  def followeds?(other_user)
    followers.include?(other_user)
  end
end
