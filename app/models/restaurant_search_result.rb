class RestaurantSearchResult < ActiveRecord::Base
	has_attached_file :res_images1,:styles => {
      :thumb => "100x100#",
      :small  => "150x150>",
      :medium => "400x200" }, default_url: "/images/missing.png"
  validates_attachment :res_images1,
  content_type: { content_type: ["image/jpeg", "image/gif", "image/png"] }
  has_attached_file :res_images2,:styles => {
      :thumb => "100x100#",
      :small  => "150x150>",
      :medium => "400x200" }, default_url: "/images/missing.png"
  validates_attachment :res_images2,
  content_type: { content_type: ["image/jpeg", "image/gif", "image/png"] }
  has_attached_file :res_images3,:styles => {
      :thumb => "100x100#",
      :small  => "150x150>",
      :medium => "400x200" }, default_url: "/images/missing.png"
  validates_attachment :res_images3,
  content_type: { content_type: ["image/jpeg", "image/gif", "image/png"] }
  has_attached_file :res_images4,:styles => {
      :thumb => "100x100#",
      :small  => "150x150>",
      :medium => "400x200" }, default_url: "/images/missing.png"
  validates_attachment :res_images4,
  content_type: { content_type: ["image/jpeg", "image/gif", "image/png"] }
  has_attached_file :res_images5,:styles => {
      :thumb => "100x100#",
      :small  => "150x150>",
      :medium => "400x200" }, default_url: "/images/missing.png"
  validates_attachment :res_images5,
  content_type: { content_type: ["image/jpeg", "image/gif", "image/png"] }
end
