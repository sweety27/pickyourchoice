class AddIdtoLocation < ActiveRecord::Migration
  def change
  	 add_column :locations, :location_cd, :string
  	 add_index :locations, :location_cd, unique: true
  end
end
