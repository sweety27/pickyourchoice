class CreateFeaturedImages < ActiveRecord::Migration
  def change
    create_table :featured_images do |t|
      t.string :image
      t.timestamps null: false
    end
  end
end
