class AddImagestoSalon < ActiveRecord::Migration
  def self.up
    change_table :salon_search_results do |t|
      t.attachment :salon_images1
      t.attachment :salon_images2
      t.attachment :salon_images3
      t.attachment :salon_images4
      
    end
  end

  def self.down
    remove_attachment :salon_search_results, :salon_images1, :salon_images2, :salon_images3, :salon_images4 
  end
end
