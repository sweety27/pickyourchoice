class AddFieldsToPg < ActiveRecord::Migration
  def change
  	add_column :search_results, :sharing_type, :string
  	add_column :search_results, :food_type, :string
  	add_column :search_results, :meals_type, :string
  end
end
