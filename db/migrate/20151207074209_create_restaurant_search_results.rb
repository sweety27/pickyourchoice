class CreateRestaurantSearchResults < ActiveRecord::Migration
  def change
    create_table :restaurant_search_results do |t|
      t.string :restname
      t.string :restaddress
      t.string :restcostfortwo
      t.string :resttype
      t.string :rating
      t.timestamps null: false
    end
  end
end
