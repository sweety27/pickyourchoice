class AddsharingTypetomany < ActiveRecord::Migration
  def change
  
  	add_reference :sharing_types, :search_result, index: true
    add_foreign_key :sharing_types, :search_results
  end
end
