class AddLocationToPg < ActiveRecord::Migration
  def change
  	add_column :search_results, :location_cd, :string
  	add_column :search_results, :location_name, :string
  end
end
