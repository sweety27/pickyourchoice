class CreateWriteReviews < ActiveRecord::Migration
  def change
    create_table :write_reviews do |t|
      t.string :clean
      t.string :food
      t.string :locality
      t.string :behavior
      t.string :amenity
      t.text :likes
      t.text :dislikes
      t.text :comment
      t.timestamps null: false
    end
  end
end
