class AddImagesToPg < ActiveRecord::Migration
  def self.up
    change_table :search_results do |t|
      t.attachment :pg_images
    end
  end

  def self.down
    remove_attachment :search_results, :pg_images
  end
end
