class AddtimetoSalon < ActiveRecord::Migration
  def change
  	add_column :salon_search_results, :work_hours, :string
  	add_column :salon_search_results, :work_days, :string
  	add_column :salon_search_results, :add_comment, :string
  end
end
