class CreateGymSearchResults < ActiveRecord::Migration
  def change
    create_table :gym_search_results do |t|
      t.string :gymname
      t.string :gymaddress
      t.string :gymcost
      t.string :gymtype
      t.string :phone
      t.string :image
      t.timestamps null: false
    end
  end
end
