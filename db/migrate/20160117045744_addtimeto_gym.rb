class AddtimetoGym < ActiveRecord::Migration
  def change
  	add_column :gym_search_results, :work_hours, :string
  	add_column :gym_search_results, :work_days, :string
  	add_column :gym_search_results, :add_comment, :string
  	add_column :gym_search_results, :location_name, :string
  end
end
