class AddImagetoRes < ActiveRecord::Migration
  def self.up
    change_table :restaurant_search_results do |t|
      t.attachment :res_images1
      t.attachment :res_images2
      t.attachment :res_images3
      t.attachment :res_images4
      t.attachment :res_images5
      
    end
  end

  def self.down
    remove_attachment :restaurant_search_results, :res_images1, :res_images2, :res_images3, :res_images4 ,:res_images5
  end
end
