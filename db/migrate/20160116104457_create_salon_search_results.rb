class CreateSalonSearchResults < ActiveRecord::Migration
  def change
    create_table :salon_search_results do |t|
      t.string :salonname
      t.string :salonaddress
      t.string :phone
      t.string :type
      t.string :image
      t.timestamps null: false
    end
  end
end
