class AddMoreImagestoPg < ActiveRecord::Migration
  def self.up
    change_table :search_results do |t|
      t.attachment :pg_images1
      t.attachment :pg_images2
      t.attachment :pg_images3
      t.attachment :pg_images4
      t.attachment :pg_images5
      t.attachment :pg_images6
    end
  end

  def self.down
    remove_attachment :search_results, :pg_images1, :pg_images2, :pg_images3, :pg_images4,  :pg_images5 , :pg_images6
  end
end
