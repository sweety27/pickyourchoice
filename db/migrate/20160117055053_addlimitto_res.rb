class AddlimittoRes < ActiveRecord::Migration
  def change
  	change_column :restaurant_search_results, :phone1, :integer ,:limit=>8
  	change_column :restaurant_search_results, :phone2, :integer ,:limit=>8
  end
end
