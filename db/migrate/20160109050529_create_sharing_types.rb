class CreateSharingTypes < ActiveRecord::Migration
  def change
    create_table :sharing_types do |t|
      t.string :typename

      t.timestamps null: false
    end
  end
end
