class Adduserstowriterevies < ActiveRecord::Migration
  def change
  	add_reference :write_reviews, :user, index: true
    add_foreign_key :write_reviews, :users
  end
end
