class AddBuffettoRes < ActiveRecord::Migration
  def change
  	add_column :restaurant_search_results, :buffet_avail, :string
  	add_column :restaurant_search_results, :buffet_price, :integer

  end
end
