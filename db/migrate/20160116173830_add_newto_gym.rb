class AddNewtoGym < ActiveRecord::Migration
  def self.up
    change_table :gym_search_results do |t|
      t.attachment :gym_images1
      t.attachment :gym_images2
      t.attachment :gym_images3
      t.attachment :gym_images4
      
    end
  end

  def self.down
    remove_attachment :gym_search_results, :gym_images1, :gym_images2, :gym_images3, :gym_images4 
  end
end
