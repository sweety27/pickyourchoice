class CreatePricelists < ActiveRecord::Migration
  def change
    create_table :pricelists do |t|
      t.integer :price

      t.timestamps null: false
    end
  end
end
