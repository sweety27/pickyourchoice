class AddFieldtoRes < ActiveRecord::Migration
  def change
  	add_column :restaurant_search_results, :food_type, :string
  	add_column :restaurant_search_results, :food_categories, :string
  	add_column :restaurant_search_results, :bar_avail, :string
  end
end
