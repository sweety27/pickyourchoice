class CreateSearchResults < ActiveRecord::Migration
  def change
    create_table :search_results do |t|
      t.string :gender
      t.string :pgname
      t.string :pgadress
      t.string :pgrent
      t.string :secdeposit
      t.string :postdate
      t.string :image
      t.timestamps null: false
    end
  end
end
