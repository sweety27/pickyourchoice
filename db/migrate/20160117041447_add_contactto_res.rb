class AddContacttoRes < ActiveRecord::Migration
  def change
  	add_column :restaurant_search_results, :phone1, :integer
  	add_column :restaurant_search_results, :phone2, :integer
  	add_column :restaurant_search_results, :location_name, :string
  end
end
