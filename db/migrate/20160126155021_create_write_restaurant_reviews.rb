class CreateWriteRestaurantReviews < ActiveRecord::Migration
  def change
    create_table :write_restaurant_reviews do |t|
      t.string :Quality
      t.string :ServiceStandards
      t.string :Ambience
      t.string :Reason
      t.string :comments
      t.string :name
      t.string :age
      t.string :email
      t.timestamps null: false
    end
  end
end
