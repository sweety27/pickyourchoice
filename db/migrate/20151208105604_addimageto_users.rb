class AddimagetoUsers < ActiveRecord::Migration
  def self.up
    change_table :users do |t|
      t.attachment :user_images
    end
  end

  def self.down
    remove_attachment :users, :user_images
  end
end
