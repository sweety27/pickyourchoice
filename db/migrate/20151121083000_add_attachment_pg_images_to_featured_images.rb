class AddAttachmentPgImagesToFeaturedImages < ActiveRecord::Migration
  def self.up
    change_table :featured_images do |t|
      t.attachment :pg_images
    end
  end

  def self.down
    remove_attachment :featured_images, :pg_images
  end
end
