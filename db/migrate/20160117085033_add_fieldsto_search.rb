class AddFieldstoSearch < ActiveRecord::Migration
  def change
  	add_column :search_results, :landmark, :string
  	add_column :search_results, :attached_washroom, :string
  	add_column :search_results, :attached_hall, :string
  end
end
