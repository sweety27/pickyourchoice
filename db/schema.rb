# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160213131106) do

  create_table "active_admin_comments", force: :cascade do |t|
    t.string   "namespace",     limit: 255
    t.text     "body",          limit: 65535
    t.string   "resource_id",   limit: 255,   null: false
    t.string   "resource_type", limit: 255,   null: false
    t.integer  "author_id",     limit: 4
    t.string   "author_type",   limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "active_admin_comments", ["author_type", "author_id"], name: "index_active_admin_comments_on_author_type_and_author_id", using: :btree
  add_index "active_admin_comments", ["namespace"], name: "index_active_admin_comments_on_namespace", using: :btree
  add_index "active_admin_comments", ["resource_type", "resource_id"], name: "index_active_admin_comments_on_resource_type_and_resource_id", using: :btree

  create_table "admin_users", force: :cascade do |t|
    t.string   "email",                  limit: 255, default: "", null: false
    t.string   "encrypted_password",     limit: 255, default: "", null: false
    t.string   "reset_password_token",   limit: 255
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          limit: 4,   default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip",     limit: 255
    t.string   "last_sign_in_ip",        limit: 255
    t.datetime "created_at",                                      null: false
    t.datetime "updated_at",                                      null: false
  end

  add_index "admin_users", ["email"], name: "index_admin_users_on_email", unique: true, using: :btree
  add_index "admin_users", ["reset_password_token"], name: "index_admin_users_on_reset_password_token", unique: true, using: :btree

  create_table "comments", force: :cascade do |t|
    t.integer  "commentable_id",   limit: 4
    t.string   "commentable_type", limit: 255
    t.string   "title",            limit: 255
    t.text     "body",             limit: 65535
    t.string   "subject",          limit: 255
    t.integer  "user_id",          limit: 4,     null: false
    t.integer  "parent_id",        limit: 4
    t.integer  "lft",              limit: 4
    t.integer  "rgt",              limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "comments", ["commentable_id", "commentable_type"], name: "index_comments_on_commentable_id_and_commentable_type", using: :btree
  add_index "comments", ["user_id"], name: "index_comments_on_user_id", using: :btree

  create_table "featured_images", force: :cascade do |t|
    t.string   "image",                  limit: 255
    t.datetime "created_at",                         null: false
    t.datetime "updated_at",                         null: false
    t.string   "pg_images_file_name",    limit: 255
    t.string   "pg_images_content_type", limit: 255
    t.integer  "pg_images_file_size",    limit: 4
    t.datetime "pg_images_updated_at"
  end

  create_table "gym_search_results", force: :cascade do |t|
    t.string   "gymname",                  limit: 255
    t.string   "gymaddress",               limit: 255
    t.string   "gymcost",                  limit: 255
    t.string   "gymtype",                  limit: 255
    t.integer  "phone",                    limit: 8
    t.string   "image",                    limit: 255
    t.datetime "created_at",                           null: false
    t.datetime "updated_at",                           null: false
    t.string   "gym_images1_file_name",    limit: 255
    t.string   "gym_images1_content_type", limit: 255
    t.integer  "gym_images1_file_size",    limit: 4
    t.datetime "gym_images1_updated_at"
    t.string   "gym_images2_file_name",    limit: 255
    t.string   "gym_images2_content_type", limit: 255
    t.integer  "gym_images2_file_size",    limit: 4
    t.datetime "gym_images2_updated_at"
    t.string   "gym_images3_file_name",    limit: 255
    t.string   "gym_images3_content_type", limit: 255
    t.integer  "gym_images3_file_size",    limit: 4
    t.datetime "gym_images3_updated_at"
    t.string   "gym_images4_file_name",    limit: 255
    t.string   "gym_images4_content_type", limit: 255
    t.integer  "gym_images4_file_size",    limit: 4
    t.datetime "gym_images4_updated_at"
    t.integer  "phone1",                   limit: 8
    t.integer  "anual_fee",                limit: 4
    t.integer  "halfyearly_fee",           limit: 4
    t.string   "work_hours",               limit: 255
    t.string   "work_days",                limit: 255
    t.string   "add_comment",              limit: 255
    t.string   "location_name",            limit: 255
  end

  create_table "locations", force: :cascade do |t|
    t.string   "name",        limit: 255
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
    t.string   "location_cd", limit: 255
  end

  add_index "locations", ["location_cd"], name: "index_locations_on_location_cd", unique: true, using: :btree

  create_table "notifications", force: :cascade do |t|
    t.integer  "write_review_id", limit: 4
    t.integer  "user_id",         limit: 4
    t.boolean  "read",            limit: 1
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
    t.string   "text",            limit: 255
  end

  add_index "notifications", ["user_id"], name: "index_notifications_on_user_id", using: :btree
  add_index "notifications", ["write_review_id"], name: "index_notifications_on_write_review_id", using: :btree

  create_table "pricelists", force: :cascade do |t|
    t.integer  "price",      limit: 4
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
  end

  create_table "relationships", force: :cascade do |t|
    t.integer  "follower_id", limit: 4
    t.integer  "followed_id", limit: 4
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
    t.integer  "user_id",     limit: 4
  end

  add_index "relationships", ["followed_id"], name: "index_relationships_on_followed_id", using: :btree
  add_index "relationships", ["follower_id", "followed_id"], name: "index_relationships_on_follower_id_and_followed_id", unique: true, using: :btree
  add_index "relationships", ["follower_id"], name: "index_relationships_on_follower_id", using: :btree

  create_table "restaurant_search_results", force: :cascade do |t|
    t.string   "restname",                 limit: 255
    t.string   "restaddress",              limit: 255
    t.string   "restcostfortwo",           limit: 255
    t.string   "resttype",                 limit: 255
    t.string   "rating",                   limit: 255
    t.datetime "created_at",                           null: false
    t.datetime "updated_at",                           null: false
    t.string   "res_images1_file_name",    limit: 255
    t.string   "res_images1_content_type", limit: 255
    t.integer  "res_images1_file_size",    limit: 4
    t.datetime "res_images1_updated_at"
    t.string   "res_images2_file_name",    limit: 255
    t.string   "res_images2_content_type", limit: 255
    t.integer  "res_images2_file_size",    limit: 4
    t.datetime "res_images2_updated_at"
    t.string   "res_images3_file_name",    limit: 255
    t.string   "res_images3_content_type", limit: 255
    t.integer  "res_images3_file_size",    limit: 4
    t.datetime "res_images3_updated_at"
    t.string   "res_images4_file_name",    limit: 255
    t.string   "res_images4_content_type", limit: 255
    t.integer  "res_images4_file_size",    limit: 4
    t.datetime "res_images4_updated_at"
    t.string   "res_images5_file_name",    limit: 255
    t.string   "res_images5_content_type", limit: 255
    t.integer  "res_images5_file_size",    limit: 4
    t.datetime "res_images5_updated_at"
    t.integer  "phone1",                   limit: 8
    t.integer  "phone2",                   limit: 8
    t.string   "location_name",            limit: 255
    t.string   "work_hours",               limit: 255
    t.string   "work_days",                limit: 255
    t.string   "add_comment",              limit: 255
    t.string   "pre_booking",              limit: 255
    t.string   "buffet_avail",             limit: 255
    t.integer  "buffet_price",             limit: 4
    t.string   "food_type",                limit: 255
    t.string   "food_categories",          limit: 255
    t.string   "bar_avail",                limit: 255
  end

  create_table "salon_search_results", force: :cascade do |t|
    t.string   "salonname",                  limit: 255
    t.string   "salonaddress",               limit: 255
    t.integer  "phone",                      limit: 8
    t.string   "type",                       limit: 255
    t.string   "image",                      limit: 255
    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
    t.string   "salon_images1_file_name",    limit: 255
    t.string   "salon_images1_content_type", limit: 255
    t.integer  "salon_images1_file_size",    limit: 4
    t.datetime "salon_images1_updated_at"
    t.string   "salon_images2_file_name",    limit: 255
    t.string   "salon_images2_content_type", limit: 255
    t.integer  "salon_images2_file_size",    limit: 4
    t.datetime "salon_images2_updated_at"
    t.string   "salon_images3_file_name",    limit: 255
    t.string   "salon_images3_content_type", limit: 255
    t.integer  "salon_images3_file_size",    limit: 4
    t.datetime "salon_images3_updated_at"
    t.string   "salon_images4_file_name",    limit: 255
    t.string   "salon_images4_content_type", limit: 255
    t.integer  "salon_images4_file_size",    limit: 4
    t.datetime "salon_images4_updated_at"
    t.integer  "salon_price",                limit: 4
    t.integer  "phone1",                     limit: 8
    t.string   "location_name",              limit: 255
    t.string   "work_hours",                 limit: 255
    t.string   "work_days",                  limit: 255
    t.string   "add_comment",                limit: 255
    t.string   "pre_booking",                limit: 255
    t.string   "gender",                     limit: 255
  end

  create_table "search_results", force: :cascade do |t|
    t.string   "gender",                   limit: 255
    t.string   "pgname",                   limit: 255
    t.string   "pgadress",                 limit: 255
    t.string   "pgrent",                   limit: 255
    t.string   "secdeposit",               limit: 255
    t.string   "postdate",                 limit: 255
    t.string   "image",                    limit: 255
    t.datetime "created_at",                           null: false
    t.datetime "updated_at",                           null: false
    t.string   "pg_images_file_name",      limit: 255
    t.string   "pg_images_content_type",   limit: 255
    t.integer  "pg_images_file_size",      limit: 4
    t.datetime "pg_images_updated_at"
    t.string   "sharing_type",             limit: 255
    t.string   "food_type",                limit: 255
    t.string   "meals_type",               limit: 255
    t.string   "location_cd",              limit: 255
    t.string   "location_name",            limit: 255
    t.string   "sharing_type1",            limit: 255
    t.string   "sharing_tyep2",            limit: 255
    t.string   "sharing_tyep3",            limit: 255
    t.string   "sharing_tyep4",            limit: 255
    t.string   "pg_images1_file_name",     limit: 255
    t.string   "pg_images1_content_type",  limit: 255
    t.integer  "pg_images1_file_size",     limit: 4
    t.datetime "pg_images1_updated_at"
    t.string   "pg_images2_file_name",     limit: 255
    t.string   "pg_images2_content_type",  limit: 255
    t.integer  "pg_images2_file_size",     limit: 4
    t.datetime "pg_images2_updated_at"
    t.string   "pg_images3_file_name",     limit: 255
    t.string   "pg_images3_content_type",  limit: 255
    t.integer  "pg_images3_file_size",     limit: 4
    t.datetime "pg_images3_updated_at"
    t.string   "pg_images4_file_name",     limit: 255
    t.string   "pg_images4_content_type",  limit: 255
    t.integer  "pg_images4_file_size",     limit: 4
    t.datetime "pg_images4_updated_at"
    t.string   "pg_images5_file_name",     limit: 255
    t.string   "pg_images5_content_type",  limit: 255
    t.integer  "pg_images5_file_size",     limit: 4
    t.datetime "pg_images5_updated_at"
    t.string   "pg_images6_file_name",     limit: 255
    t.string   "pg_images6_content_type",  limit: 255
    t.integer  "pg_images6_file_size",     limit: 4
    t.datetime "pg_images6_updated_at"
    t.string   "price",                    limit: 255
    t.string   "price1",                   limit: 255
    t.string   "price2",                   limit: 255
    t.string   "price3",                   limit: 255
    t.string   "price4",                   limit: 255
    t.string   "gym_images1_file_name",    limit: 255
    t.string   "gym_images1_content_type", limit: 255
    t.integer  "gym_images1_file_size",    limit: 4
    t.datetime "gym_images1_updated_at"
    t.string   "gym_images2_file_name",    limit: 255
    t.string   "gym_images2_content_type", limit: 255
    t.integer  "gym_images2_file_size",    limit: 4
    t.datetime "gym_images2_updated_at"
    t.string   "gym_images3_file_name",    limit: 255
    t.string   "gym_images3_content_type", limit: 255
    t.integer  "gym_images3_file_size",    limit: 4
    t.datetime "gym_images3_updated_at"
    t.string   "gym_images4_file_name",    limit: 255
    t.string   "gym_images4_content_type", limit: 255
    t.integer  "gym_images4_file_size",    limit: 4
    t.datetime "gym_images4_updated_at"
    t.integer  "phone1",                   limit: 8
    t.integer  "phone2",                   limit: 8
    t.string   "work_hours",               limit: 255
    t.string   "amenties",                 limit: 255
    t.string   "add_comment",              limit: 255
    t.string   "landmark",                 limit: 255
    t.string   "attached_washroom",        limit: 255
    t.string   "attached_hall",            limit: 255
  end

  create_table "sharing_types", force: :cascade do |t|
    t.string   "typename",         limit: 255
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
    t.string   "type_cd",          limit: 255
    t.integer  "search_result_id", limit: 4
  end

  add_index "sharing_types", ["search_result_id"], name: "index_sharing_types_on_search_result_id", using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "email",                    limit: 255, default: "", null: false
    t.string   "encrypted_password",       limit: 255, default: "", null: false
    t.string   "reset_password_token",     limit: 255
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",            limit: 4,   default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip",       limit: 255
    t.string   "last_sign_in_ip",          limit: 255
    t.datetime "created_at",                                        null: false
    t.datetime "updated_at",                                        null: false
    t.integer  "phno",                     limit: 8
    t.string   "provider",                 limit: 255
    t.string   "uid",                      limit: 255
    t.string   "username",                 limit: 255
    t.string   "user_images_file_name",    limit: 255
    t.string   "user_images_content_type", limit: 255
    t.integer  "user_images_file_size",    limit: 4
    t.datetime "user_images_updated_at"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["phno"], name: "index_users_on_phno", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

  create_table "votes", force: :cascade do |t|
    t.integer  "votable_id",   limit: 4
    t.string   "votable_type", limit: 255
    t.integer  "voter_id",     limit: 4
    t.string   "voter_type",   limit: 255
    t.boolean  "vote_flag",    limit: 1
    t.string   "vote_scope",   limit: 255
    t.integer  "vote_weight",  limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "votes", ["votable_id", "votable_type", "vote_scope"], name: "index_votes_on_votable_id_and_votable_type_and_vote_scope", using: :btree
  add_index "votes", ["voter_id", "voter_type", "vote_scope"], name: "index_votes_on_voter_id_and_voter_type_and_vote_scope", using: :btree

  create_table "write_restaurant_reviews", force: :cascade do |t|
    t.string   "Quality",          limit: 255
    t.string   "ServiceStandards", limit: 255
    t.string   "Ambience",         limit: 255
    t.string   "Reason",           limit: 255
    t.string   "comments",         limit: 255
    t.string   "name",             limit: 255
    t.string   "age",              limit: 255
    t.string   "email",            limit: 255
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
  end

  create_table "write_reviews", force: :cascade do |t|
    t.string   "clean",      limit: 255
    t.string   "food",       limit: 255
    t.string   "locality",   limit: 255
    t.string   "behavior",   limit: 255
    t.string   "amenity",    limit: 255
    t.text     "likes",      limit: 65535
    t.text     "dislikes",   limit: 65535
    t.text     "comment",    limit: 65535
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
    t.integer  "user_id",    limit: 4
  end

  add_index "write_reviews", ["user_id"], name: "index_write_reviews_on_user_id", using: :btree

  add_foreign_key "notifications", "users"
  add_foreign_key "notifications", "write_reviews"
  add_foreign_key "sharing_types", "search_results"
  add_foreign_key "write_reviews", "users"
end
