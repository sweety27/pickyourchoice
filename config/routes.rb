  Rails.application.routes.draw do
  get 'gym/index'

  get 'salon/index'

  get 'restaurants/index'

  get 'result/index'

  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
  get 'home/index'
  
  devise_for :users ,:controllers => { :omniauth_callbacks => "users/omniauth_callbacks" } do
  get '/users/sign_in' => 'devise/sessions#new'
  get '/users/sign_out' => 'devise/sessions#destroy'
    
end
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".
resources :users  do
  resources :write_review  
  
    member do
      get :following, :followers
    end
  end
resources :featured_images
resources :relationships  
resources :locations , :defaults => {:format => "json"}
get '/location.json' => 'home#index', :defaults => {:format => 'json'}
get '/resultlocation.json' => 'result#index', :defaults => {:format => 'json'}
get '/review.json' => 'result#reviewdetails', :defaults => {:format => 'json'}
get '/userreviews.json' => 'relationships#index', :defaults => {:format => 'json'}
get '/likes.json' => 'result#like', :defaults => {:format => 'json'}
get '/dislikes.json' => 'result#dislike', :defaults => {:format => 'json'}
get '/pgresults.json' => 'result#pgresult', :defaults => {:format => 'json'}
get '/pgresultsbyfood.json' => 'result#pgresultbyfoodtype', :defaults => {:format => 'json'}
get '/restaurantresults.json' => 'restaurants#restaurantresult', :defaults => {:format => 'json'}
get 'relationships/show' 
get '/salonresults.json' => 'salon#salonresult', :defaults => {:format => 'json'}
  # You can have the root of your site routed with "root"
   root 'home#index'
    post '/create' => 'home#create' 
    

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end

   match '/phonenum' => 'home#phonenum', :via => 'get'
   match '/result' => 'result#index', :via => 'get'
   
   
   get '/review' => 'result#completereview'
   match '/writereview' => 'result#write_review', :via => 'get'
   match '/restaurants' => 'restaurants#index', :via => 'get'
    post '/createreview' => 'result#create' 
    post '/createrestreview' => 'restaurants#create'
    get '/like/:id' => 'result#like' ,as: 'like'
    get '/dislike/:id' => 'result#dislike' ,as: 'dislike'
    post '/createfollower' => 'relationships#create'
    delete '/deletefollower' => 'relationships#destroy'
  match '/followuser' => 'relationships#show', :via => 'get'
  match '/details' => 'detail#details', :via => 'get'
  match '/restaurant_details' => 'restaurant_detail#restaurant_details', :via => 'get'
  match '/salon' => 'salon#index', :via => 'get'
  match '/salon_details' => 'salon_detail#salon_details', :via => 'get'
  match '/gym' => 'gym#index', :via => 'get'
  match '/writerestaurantreview' => 'restaurants#restwritereview', :via => 'get'

end
